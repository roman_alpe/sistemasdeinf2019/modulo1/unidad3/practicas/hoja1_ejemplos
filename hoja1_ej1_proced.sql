﻿DROP DATABASE IF EXISTS ej_programacion1;
CREATE DATABASE ej_programacion1;
USE ej_programacion1;

-- Hoja de ejemplos (1)

-- Opción 1
-- Con la instrucción if
DROP PROCEDURE IF EXISTS nombre;
CREATE PROCEDURE nombre(a int, b int)
  BEGIN
    IF a>b THEN
      SELECT a;
    ELSE
      SELECT b;
    END IF; 
  END;

CALL nombre(12,23);

-- Opción 2
-- Con una tabla temporal y una consulta de totales
DROP PROCEDURE IF EXISTS ej1v1b;
CREATE PROCEDURE ej1v1b(a int, b int)
  BEGIN 
    CREATE TEMPORARY TABLE IF NOT EXISTS numeros(
      numero int
      );
      INSERT numeros(numero) VALUES (a),(b);
      SELECT * FROM numeros;
      SELECT MAX(numero) FROM numeros;
    DROP TABLE numeros;
  END;

  CALL ej1v1b(12,55);

-- Creación de tabla temporal
CREATE TEMPORARY TABLE prueba(
  pitufito int);
);
SELECT * FROM prueba p;

-- Opción 3
-- Con la función de mysql GREATEST

DROP PROCEDURE IF EXISTS ej1v1c;
CREATE PROCEDURE ej1v1c(a int, b int)
  BEGIN
    SELECT GREATEST(a,b);
  END;

CALL ej1v1c(22,50);



-- Ejercicio 2
-- Opción 1
DROP PROCEDURE IF EXISTS ej2v1a;
CREATE PROCEDURE ej2v1a(a int, b int, c int)
  BEGIN
    IF a>b  THEN
      IF a>c  THEN
        SELECT a;
      ELSE 
        SELECT c;
      END IF;
    ELSE 
        IF (b>c) THEN
          SELECT b;
        ELSE
          SELECT c;
        END IF;               
     END IF;
   END; 


CALL ej2v1a(81,92,75);

-- Opción 2

DROP PROCEDURE IF EXISTS ej2v2;
CREATE PROCEDURE ej2v2(a int, b int, c int)
  
  BEGIN 
    CREATE TEMPORARY TABLE IF NOT EXISTS t1(
      id int AUTO_INCREMENT PRIMARY KEY

      num1 int
      );
      
      INSERT INTO t1(num1) VALUES 
        
      (DEFAULT, a),
      (DEFAULT, b),
      (DEFAULT, c);
      SELECT MAX(num1) FROM t1;
    
    DROP TABLE t1;
  END;

  CALL ej2v2(21,52,75);



-- Con la función GREATEST

DROP PROCEDURE IF EXISTS ej2v2a;
CREATE PROCEDURE ej2v2a(a int, b int, c int)
  BEGIN
    SELECT GREATEST(a,b,c);
  END;

  CALL ej2v2a(12,23,55);

/** Ejercicio 3

**/

 
DROP PROCEDURE IF EXISTS ej3;
CREATE PROCEDURE ej3(a int, b int, c int, OUT mayor int, IN menor int)
  BEGIN
    SET mayor= GREATEST(a,b,c);
    SET menor= LEAST(a,b,c);

  END;

  CALL ej3(15,35,75);
  SELECT (@v1,@v2);


/** Ejercicio 4
**/
DROP PROCEDURE IF EXISTS ej4;
CREATE PROCEDURE ej4(fecha1 date, fecha2 date)
  BEGIN
  
    SELECT DATEDIFF(fecha1, fecha2); 

  END;
  CALL ej4('2010/1/1','2010/1/10');

/** Ejercicio 5
**/

DROP PROCEDURE IF EXISTS ej5;
CREATE PROCEDURE ej5(fecha1 date, fecha2 date)
  BEGIN
  
    SELECT TIMESTAMPDIFF(MONTH, fecha1, fecha2); 

  END;
  CALL ej5('2010/1/1','2010/1/10');


/** Ejercicio 6
**/
DROP PROCEDURE IF EXISTS ej6;
CREATE PROCEDURE ej6(fecha1 date, fecha2 date, OUT dias int, OUT meses int, OUT annos int)
  BEGIN
  
    SET dias=TIMESTAMPDIFF(DAY,fecha1,fecha2); 
    
    SET meses=TIMESTAMPDIFF(MONTH,fecha1,fecha2);
    
    SET annos=TIMESTAMPDIFF(YEAR,fecha1,fecha2); 


  END;
  
  CALL ej6('2010/1/1','2010/1/10',@d,@m,@a);
  SELECT @d, @m, @a;

/**
ACTUALIZAR REGISTROS DE UNA TABLA
---------------------------------
CREATE TABLE datos1(

id int AUTO_INCREMENT PRIMARY KEY,
suma int,
numero1 int;
numero int);

INSERT INTO datos1 (numero1, numero)
  VALUES (8,7);

SELECT * FROM datos1;

UPDATE datos1 d
  SET d.suma=d.numero1+d.numero
  WHERE suma is null;

**/

/** Ejercicio 7
**/
DROP PROCEDURE IF EXISTS ej7;
CREATE PROCEDURE ej7(IN frase varchar(150))
  BEGIN
  
    SELECT CHAR_LENGTH(frase);
     
  END;
  
  CALL ej7('helicoptero');
  